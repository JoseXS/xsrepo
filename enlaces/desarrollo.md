# Desarrollo
#### 1. [Angular](#angular)
#### 2. [APIS](#apis)
#### 3. [Blogs](#blogs)
#### 4. [Ionic](#ionic)
#### 5. [Kotlin](#kotlin)
#### 6. [Python](#python)
#### 7. [Recursos](#recursos)
#### 8. [Varios](#varios)

## Angular
- [Stackblitz](https://stackblitz.com/) Desarrollo de Angular online
- [Awesome Angular](https://github.com/gdi2290/awesome-angular)
- [Awesome Angular Components](https://github.com/brillout/awesome-angular-components)
- [Testing](https://codecraft.tv/courses/angular/unit-testing/components/)
- [Lighhouse 100 points](https://dzone.com/articles/angular5-pwa-with-perfect-100-lighthouse-points)
- [Angular Universal + Firebase functions](https://enriqueoriol.com/blog/2018/04/angular-universal-firebase-functions.html)

## APIS
- [Api Gobierno España](http://datos.gob.es/es) Gratis
- [Api Race](http://mapas.race.es/WebServices/srvRace.asmx/ObtenerDatos?pstrIncidencias=0&pstrCamaras=1&pstrRadares=0&pstrGasolineras=0&pstrPuntosNegros=0&pstrParking=0) Realmente es la respuesta en XML a todos los camaras, incidencias, gasolineras, puntos negros y radares de la DGT / Race (Casi una api...)
- [Api Spotify Console](https://developer.spotify.com/console/get-search-item/)
- [Postman Collections](https://postmancollections.com/) Colecciones para postman con apis famosas

## Blogs
- [VictorRobles](https://victorroblesweb.es/) Profesor de Angular / Javascript en Udemy

## Ionic
- [Overriding Ionic Sass variables](https://ionicframework.com/docs/theming/overriding-ionic-variables/) Informacion sobre las variables sass de Ionic
- [Plugins Ionic Market](https://market.ionicframework.com/plugins) Algunos gratis, otros no...
- [Awesome Ionic](https://github.com/Alexintosh/Awesome-Ionic)

## Kotlin - Android
- [Curso Android – Java Udemy](https://www.udemy.com/programacion-de-android-desde-cero/learn/v4/content)
- [Documentación oficial de Kotlin](https://kotlinlang.org/docs/reference)
- [Primeros pasos en Kotlin](https://blog.kirei.io/quieres-aprender-kotlin-empieza-por-aqu%C3%AD-85fb4f481860)
- [Guia Material de Google](https://material.io/)
- [From Java to Klotin](https://fabiomsr.github.io/from-java-to-kotlin/)
- [Articulo para principiantes](https://blog.kirei.io/quieres-aprender-kotlin-empieza-por-aqu%C3%AD-85fb4f481860)
- Articulos para practicar (Refactorizacion de codigo)
  - [Testing like a pro in kotlin](http://blog.karumi.com/testing-like-a-pro-in-kotlin/)
  - [Movies kotlin kata](https://github.com/xurxodev/Movies-Kotlin-Kata)
- Proyectos Kotlin para probar
  - [Kotlin for android developers](https://github.com/antoniolg/Kotlin-for-Android-Developers)
  - [Bandhook Kotlin](https://github.com/antoniolg/Bandhook-Kotlin)
  - [MovieGuide Kotlin](https://github.com/esoxjem/MovieGuide-Kotlin)
  - [Simple calendar](https://github.com/SimpleMobileTools/Simple-Calendar)
  - [Twidere Android](https://github.com/TwidereProject/Twidere-Android)
- [Aplicación paso a paso](https://android.jlelse.eu/learn-kotlin-while-developing-an-android-app-introduction-567e21ff9664?gi=23b63818af6a )
- [Recursos para Kotlin](https://kotlin.link/)
- [Iconos Material](https://icons8.com/material-icons/)
- [Material Colors](http://materialcolors.com/)
- [Material Palette](https://www.materialpalette.com/)

## Python
- [Awesome Python](https://github.com/vinta/awesome-python)

## Recursos
- [Textcraft](https://textcraft.net/) Imagenes tipo Minecraft
- [PageSpeed](https://developers.google.com/speed/pagespeed/insights/)


## Varios
- [DevDocs](http://devdocs.io/) Documentacion de diferentes lenguajes
- [MaterialDesignKit](https://materialdesignkit.com/templates/)

----------------------------

[Volver](./README.md)