# 5) Varios
#### 1. [Camaras](#camaras)
#### 2. [Sin catalogar](#sin-catalogar)

## Camaras - Live Streaming
- [Insecam](https://www.insecam.org)
- [OpenTopia](http://www.opentopia.com)

## Sin catalogar
- [Planecrashinfo](http://www.planecrashinfo.com/) Info de aviones siniestrados
- [Texas Departament of Criminal Justice](https://www.tdcj.state.tx.us/death_row/dr_executed_offenders.html) Mensajes de presos antes de morir
- [Sentimentalcorp](http://sentimentalcorp.org) Videos raros de algun loco
- [Google Contacts Maps](https://script.google.com/macros/s/AKfycbze0VyIYgUzjvA5dlcwPdayxUFeSDrAKWizTXPxN7Nu4NNINtc/exec?map=1) Mapa con los contactos de Google (No es oficial)

----------------------------

[Volver](./README.md)