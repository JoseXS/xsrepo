# A
- ##### Angular.json
    Es el archivo de configuracion de Angular 6. Anteriormente se llamaba angular-cli.json

- ##### Apache
    Servidor web muy conocido multiplataforma

- ##### Angular
    Framework creado por Google pensado en SPA

# B
- ##### Bootstrap
    Framework CSS de Twitter

# C
- ##### ChartJS
    Libreria para NodeJS para crear graficas para estadisticas

# E
- ##### ExpressJS
    Libreria para NodeJS, para aplicaciones web

# I
- ##### Ionic
    Framework para la creacion de aplicaciones moviles. Se desarrolla con Angular

# J
- ##### JQuery
    Libreria para Javascript de las mas conocidas y completas que hay. Se ha quedado un poco obsoleta pero sigue funcionando a la perfeccion.

# L
- ##### Letsencrypt
    Generador de certificados para HTTPS en servidores

- ##### LAMP
    Linux + Apache + Mysql + Php / Perl / Python

# M
- ##### MongoDB
    Base de datos no estructural.

# N
- ##### Ng
    Sirve para lanzar comandos en Angular CLI

- ##### NVM
    Gestor de versiones de NodeJS

- ##### Nano
    Editor de texto para distribuciones Ubuntu basadas en Debian

- ##### NodeJS
    Sirve para usar Javascript en el lado del servidor

- ##### Nginx
    Servidor y proxy web

# P
- ##### Pm2
    Paquete para NodeJS, para administrar aplicaciones y su recarga automatica en caso de error

- ##### Pipe
    Tuberia, pedazo de codigo, que despues puedes incluir en el html de angular con su nombre

# S
- ##### SPA
    Single page application. Aplicacion de una sola pagina (Sin recargar)

- ##### Snippet
    Fragmento de codigo de programacion

- ##### Sudo
    Comando de administrador de distribuciones Debian, entre ellas Ubuntu

- ##### SSH
    Protocolo de conexion remota a traves de linea de comandos. En window se suele utilizar Putty, y en linux ya viene de serie.

# T
- ##### Typescript
    Lenguaje de programacion creado por Microsoft. Es como Javascript pero vitaminado.

# U
- ##### Ubuntu
    Sistema operativo de la empresa canonical. Es gratis y esta basado en Linux.

# V
- ##### VPS
    Es un servidor virtual privado. Sirve para particionar un servidor fisico en varios servidores. Asi pues cada uno tiene su cacho del servidor.

