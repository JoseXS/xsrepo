# Crear SSH key y vincularla a un servidor

- Creamos la key, la va a crear en /home/tuusuario/.ssh/

`` ssh-keygen ``

- Copiamos la key y la vinculamos a un usuario en un servidor

`` ssh-copy-id josexs@josexs.com ``

- Pedira la contraseña del usuario del servidor y despues ya podemos entrar sin contraseñas


--------

[Volver](./README.md)