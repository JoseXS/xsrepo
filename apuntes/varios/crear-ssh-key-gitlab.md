# Crear ssh key Gitlab

- Localizar si tenemos una ssh key

`` cat ~/.ssh/id_rsa.pub ``

- Si no la tenemos ya, creamos la ssh key

`` ssh-keygen -o -t rsa -C "nombre@gmail.com" -b 4096 ``

- Copiamos la ssh key

`` cat ~/.ssh/id_rsa.pub ``

- Vamos a [Gitlab](https://gitlab.com/profile/keys) y añadimos la key

- Despues hay que clonar el repositorio de nuevo ya que si no, nos seguirá pidiendo la password

----------

[Volver](./README.md)
