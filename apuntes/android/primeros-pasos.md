# Primeros pasos

## Sintaxis 
 - Siempre se empieza con un public class y con un  public static void main(String[] args) 

## Operadores de comparacion 
`` == != > < <= >= `` 


## Ejemplo hola mundo 
```
public class HelloWorld { 
  public static void main(String[] args) { 
    System.out.println("Hola Mundo"); 
  } 
} 
```

## Variables 

- Numeros: int, float, double 
- Caracteres: char, string 
- Booleanos: Boolean 
- Se reserva un espacio en la memoria dependiendo el tipo de variable 
- Zona de memoria donde se almacena el dato

![alt text](https://image.ibb.co/k43GML/Get-Image-1.jpg "Variables")


## Arrays 
- Int pagina[] = new int[6] 


## Matrices 

- String tablero[][] = new String[8][8]; 

- tablero[5][4] = "Rey negro"; 

- System.out.println(tablero[5][4]); 


## Switch 

Permite hacer varias comprobaciones de un mismo dato 

- Con break paras el caso 

- Con default si no existe el caso, se muestra el default.  

- Antes de llegar al switch tienes que revisar que los datos esten correctos, ya que si en el siguiente caso añades a numero 250 te saldria 28 

``` 

int numero = 5 

switch(numero) { 
  case 12: System.out.println("Tiene 31 dias"); 
  break;
  case 11: System.out.println("Tiene 30 dias"); 
  break;  
  default: System.out.println("Tiene 28 dias");     
} 

 ```

 

## While 
```
int num = 3; 

int i = 1; 

while (i <= 10) { 

System.out.println(num + " * " + i + " = " + num * i); 

i ++; 

} 
```
 

// do while 

 ```
do { 

System.out.println(num + " * " + i + " = " + num * i); 

i ++; 

} while (i <= 10); 
```



## For 

    el valor con el q comienza el iterador (i = 1) 

    condicion q tiene q cumplirse para q se siga ejecutando (i <= 10 

    como cambia el iterador cada vez que se ejecuta el bucle (i++) 

 

## Clases 

 ```
Person p = new Person ("Josito"); 

public class Person{ 
  public String name; 
  Person(String name){ 
  this.name = name; 
  } 
} 
```

## Activity 

La ventana de la aplicacion que tenemos en Android. Tenemos asignado una parte de codigo de parte logica y otra de diseño grafico, para montar la parte visual. 

 

## Context 

Un activity tiene siempre un context y solo puede tener uno. Nos vale para decirle donde renderizar las cosas, lanzar otros activity... 

Se importan asi: 

```
Activity activity = this; 

Context context = this; 
```

 

## Ciclos de vida 
![alt text](https://image.ibb.co/gii6O0/ciclosdevida.png "Ciclos de vida")

![alt text](https://preview.ibb.co/mrkWqf/Captura-de-pantalla-2018-03-18-a-las-15-45-03.png)


@Override protected void onResume(){
	super.onResume();
	Toast.makeText(this,"onResume",Toast.LENGTH_LONG).show();
}
@Override protected void onPause(){
	super.onPause();
	Toast.makeText(this,"onPause",Toast.LENGTH_LONG).show();
}
@Override protected void onStop(){
	super.onStop();
	Toast.makeText(this,"onStop",Toast.LENGTH_LONG).show();
}
@Override protected void onDestroy(){
	super.onDestroy();
}

----

[Volver](../README.md)



    