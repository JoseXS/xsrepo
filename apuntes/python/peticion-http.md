# Peticion Http
Vamos a utilizar la libreria requests. Si no tenemos la instalamos `pip install requests`

```
import requests

req = requests.get('https://randomuser.me/api/')

print(req.json())
```

----

[Volver](./README.md)