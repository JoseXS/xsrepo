# Tips
| Comando                              | Descripcion                   |
| ------------------------------------ | ----------------------------- |
| isolation.tools.copy.disable: false  | Se habilita copiar en consola |
| isolation.tools.paste.disable: false | Se habilita pegar en consola  |

----------------------------

[Volver](./README.md)