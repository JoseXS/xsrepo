# Librerias utiles para NodeJS

- [Librerias utiles para NodeJS](#librerias-utiles-para-nodejs)
    - [Api Rest](#api-rest)
    - [Bases de datos](#bases-de-datos)
    - [Canvas](#canvas)
    - [Fechas](#fechas)
    - [Formatos](#formatos)
    - [Gestion de procesos](#gestion-de-procesos)
    - [Imagenes](#imagenes)
    - [Logins](#logins)
    - [Minificado](#minificado)
    - [Revisores de codigo (Linters)](#revisores-de-codigo-linters)
    - [Servidores web](#servidores-web)
    - [Sockets](#sockets)
    - [Testing](#testing)
    - [Utilidades](#utilidades)
    - [Validadores](#validadores)
    - [Varios](#varios)

---

## Api Rest
| Libreria  | Descripcion          | Url                                            |
| --------- | -------------------- | ---------------------------------------------- |
| ExpressJS | Creacion de REST Api | [Url](https://github.com/expressjs/express)    |
| Restify   | Creacion de REST Api | [Url](https://github.com/restify/node-restify) |

---

## Bases de datos
| Libreria | Descripcion        | Url                                           |
| -------- | ------------------ | --------------------------------------------- |
| Mongoose | Gestion de MongoDB | [Url](https://github.com/Automattic/mongoose) |
| Mysql    | Gestion de MYSQL   | [Url](https://github.com/mysqljs/mysql)       |

---

## Canvas
| Libreria     | Descripcion                    | Url                                           |
| ------------ | ------------------------------ | --------------------------------------------- |
| FabricJS     | Gestion de canvas mas amigable | [Url](https://github.com/fabricjs/fabric.js/) |
| Canvas2image | Guardar canvas en imagen       | [Url](https://github.com/hongru/canvas2image) |

---

## Fechas
| Libreria | Descripcion        | Url                                     |
| -------- | ------------------ | --------------------------------------- |
| Moment   | Formato de fechas. | [Url](https://github.com/moment/moment) |

---

## Formatos
| Libreria | Descripcion                             | Url                                         |
| -------- | --------------------------------------- | ------------------------------------------- |
| PDFKit   | Generador de PDF                        | [URL](https://github.com/foliojs/pdfkit)    |
| jsPDF    | Generador de PDF                        | [URL](https://github.com/MrRio/jsPDF)       |
| Cheerio  | Para trabajar con DOM desde el servidor | [URL](https://github.com/cheeriojs/cheerio) |
| NodeCSV  | Procesar archivos CSV                   | [URL](https://github.com/adaltas/node-csv)  |
| Marked   | Procesar archivos Markdown              | [Url](https://github.com/markedjs/marked)   |

---

## Gestion de procesos
| Libreria | Descripcion                    | Url                                   |
| -------- | ------------------------------ | ------------------------------------- |
| PM2      | Administra aplicaciones nodejs | [Url](https://github.com/Unitech/pm2) |
--

## Imagenes
| Libreria       | Descripcion              | Url                                            |
| -------------- | ------------------------ | ---------------------------------------------- |
| GraphicsMagick | Manipulacion de imagenes | [Url](https://github.com/aheckmann/gm)         |
| Sharp          | Procesado de imagenes    | [Url](https://github.com/lovell/sharp)         |
| Spritesmith    | Generar hojas de sprites | [Url](https://github.com/twolfson/spritesmith) |

---

## Logins
| Libreria | Descripcion                             | Url                                            |
| -------- | --------------------------------------- | ---------------------------------------------- |
| Passport | Gestion de login con servicios externos | [Url](https://github.com/jaredhanson/passport) |

---

## Minificado
| Libreria     | Descripcion        | Url                                                |
| ------------ | ------------------ | -------------------------------------------------- |
| Imagemin     | Minificar imagenes | [Url](https://github.com/imagemin/imagemin)        |
| HTMLMinifier | Minificar HTML     | [Url](https://github.com/kangax/html-minifier)     |
| Clean-css    | Minificar CSS      | [Url](https://github.com/jakubpawlowicz/clean-css) |
| UglifyJS     | Minificar JS       | [Url](https://github.com/mishoo/UglifyJS2)         |
| SVGO         | Minificar SVG      | [Url](https://github.com/svg/svgo)                 |

---

## Revisores de codigo (Linters)
| Libreria | Descripcion                   | Url                                     |
| -------- | ----------------------------- | --------------------------------------- |
| JSHint   | Detector de errores en codigo | [Url](https://github.com/jshint/jshint) |

---

## Servidores web
| Libreria    | Descripcion                                 | Url                                             |
| ----------- | ------------------------------------------- | ----------------------------------------------- |
| Http-server | Para lanzar un servidor, http-server -c-1 . | [Url](https://github.com/indexzero/http-server) |

---

## Sockets
| Libreria | Descripcion                                         | Url                                          |
| -------- | --------------------------------------------------- | -------------------------------------------- |
| Socket   | Gestion de eventos en tiempo real (Util para chats) | [Url](https://github.com/socketio/socket.io) |

---

## Testing
| Libreria | Descripcion       | Url                                          |
| -------- | ----------------- | -------------------------------------------- |
| Mocha    | Creacion de tests | [Url](https://github.com/mochajs/mocha)      |
| Karma    | Creacion de tests | [Url](https://github.com/karma-runner/karma) |

---

## Utilidades
| Libreria    | Descripcion                  | Url                                             |
| ----------- | ---------------------------- | ----------------------------------------------- |
| Winston     | Logger                       | [Url](https://github.com/winstonjs/winston)     |
| Faker       | Fake data                    | [Url](https://github.com/Marak/Faker.js)        |
| Nodemailer  | Envio de emails              | [Url](https://github.com/nodemailer/nodemailer) |
| CommanderJS | Creacion de aplicaciones CLI | [Url](https://github.com/tj/commander.js)       |

---

## Validadores
| Libreria     | Descripcion          | Url                                           |
| ------------ | -------------------- | --------------------------------------------- |
| Validator.js | Validador de strings | [Url](https://github.com/chriso/validator.js) |

---

## Varios
| Libreria        | Descripcion                                    | Url                                           |
| --------------- | ---------------------------------------------- | --------------------------------------------- |
| Cost of Modules | Para saber el tamaño de los modulos instalados | [Url](https://github.com/siddharthkp/cost-of-modules) |

[Volver](./README.md)