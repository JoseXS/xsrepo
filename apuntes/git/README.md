# Git

- [Git](#git)
    - [Comandos basicos](#comandos-basicos)
    - [Crear ssh key y vincularla con gitlab](../varios/crear-ssh-key-gitlab.md)


## Comandos basicos
| Comando           | Descripcion        |
| ----------------- | ------------------ |
| git checkout -- . | Descartar cambios |
|                   |                    |

----------------------------

[Volver](../README.md)