# Configuracion App
En el app.module.ts podemos modificar ciertas parametros de la aplicación. 
Para modificarlas incluimos dentro de imports los cambios que necesitemos, por ejemplo:

```
IonicModule.forRoot(MyApp, {
	tabsPlacement: 'top',
	tabsLayout: 'title-hide',
	backButtonText: 'Atras',
	iconMode: 'md',
	modalEnter: 'modal-slide-in',
	modalLeave: 'modal-slide-out',
	pageTransition: 'ios-transition',
	activator: 'ripple',
	tabsHighlight: false,
	platforms: {
		ios: {
			tabsPlacement: 'top',
		}
	}
})
```

| **Comando**            | **Descripcion**                                                                                        |
| ---------------------- | ------------------------------------------------------------------------------------------------------ |
| **activator**          | Usado para botones, cambia el efecto de presionar un botón. Opciones disponibles: "ripple","highlight" |
| **actionSheetEnter**   | El nombre de la transición para usar mientras se presenta una hoja de acción.                          |
| **actionSheetLeave**   | El nombre de la transición a usar mientras se descarta una hoja de acción.                             |
| **alertEnter**         | El nombre de la transición a usar mientras se presenta una alerta.                                     |
| **alertLeave**         | El nombre de la transición para usar mientras se descarta una alerta.                                  |
| **backButtonText**     | El texto para mostrar por el ícono de botón de retroceso en la barra de navegación.                    |
| **backButtonIcon**     | El ícono para usar como ícono del botón Atrás.                                                         |
| **iconMode**           | El modo a usar para todos los íconos en toda la aplicación. Opciones: "ios","md"                       |
| **locationStrategy**   | Establézcalo en 'ruta' para eliminar hashbangs cuando use Deeplinking.                                 |
| **loadingEnter**       | El nombre de la transición a usar mientras se presenta un indicador de carga.                          |
| **loadingLeave**       | El nombre de la transición a usar mientras se descarta un indicador de carga.                          |
| **menuType**           | Tipo de menú para mostrar. Opciones: "overlay","reveal", "push".                                       |
| **modalEnter**         | El nombre de la transición para usar mientras se presenta un modal.                                    |
| **modalLeave**         | El nombre de la transición para usar mientras que un modal se descarta.                                |
| **mode**               | El modo a usar en toda la aplicación.                                                                  |
| **pageTransition**     | El nombre de la transición para usar al cambiar de página.                                             |
| **pickerEnter**        | Se presenta el nombre de la transición a usar mientras se selecciona un selector.                      |
| **pickerLeave**        | El nombre de la transición para usar mientras se selecciona un selector.                               |
| **popoverEnter**       | El nombre de la transición para usar mientras se muestra un popover.                                   |
| **popoverLeave**       | El nombre de la transición para usar mientras se elimina un popover.                                   |
| **spinner**            | El spinner predeterminado para usar cuando un nombre no está definido.                                 |
| **swipeBackEnabled**   | Si la función de deslizamiento nativo de iOS para volver está habilitada.                              |
| **tabsHighlight**      | Si se muestra una línea resaltada debajo de la pestaña cuando se selecciona.                           |
| **tabsLayout**         | El diseño que se usará para todas las pestañas.                                                        |
| **tabsPlacement**      | La posición de las pestañas en relación con el contenido. Opciones: "top","bottom"                     |
| **tabsHideOnSubPages** | Ya sea para ocultar las pestañas en las páginas secundarias o no.                                      |
| **toastEnter**         | El nombre de la transición para usar mientras se presenta un brindis.                                  |
| **toastLeave**         | El nombre de la transición para usar mientras se brinda un brindis se descarta.                        |

[Volver](./README.md)