# Directiva para Imagenes fallidas (Image-preload)

Basicamente se trata de una directiva ``(default)`` que le añadiremos a la etiqueta ``<img>`` en el html para que cuando falle la imagen, se cambie automaticamente por la que proporcionemos



### Directiva
Creamos el archivo ``image-preload.directive.ts``
```
import { Directive, Input, HostBinding } from '@angular/core';
@Directive({
    selector: 'img[default]',
    host: {
        '(error)': 'updateUrl()',
        '(load)': 'load()',
        '[src]': 'src'
    }
})

export class ImagePreloadDirective {
    @Input() src: string;
    @Input() default: string;
    @HostBinding('class') className;

    updateUrl() {
        this.src = this.default;
    }
    load() {
        this.className = 'image-loaded';
    }
}
```

### Importacion
Lo importamos en el archivo ``app.module.ts`` dentro de ``declarations: []``

### Utilizacion
En la parte del html, dentro de una etiqueta img, añadimos el atributo default con una direccion que tengamos en local
```
<img default="assets/images/logo.png" src="{{url}}">
```

[Volver](./README.md)