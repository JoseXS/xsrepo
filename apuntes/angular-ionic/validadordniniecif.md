## Validador de DNI, NIE, CIF

Servicio para validar documentos de identidad

```
import { Injectable } from '@angular/core';

@Injectable()
export class ValidateDNINIECIFProvider {
    DNI_REGEX = /^(\d{8})([A-Z])$/;
    CIF_REGEX = /^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/;
    NIE_REGEX = /^[XYZ]\d{7,8}[A-Z]$/;

    constructor() { }

    validateDocumentID(str): any {
        str = str.toUpperCase().replace(/\s/, '');
        let valid = false;
        const type = this.documentType(str);

        switch (type) {
            case 'dni': valid = this.validDNI(str); break;
            case 'nie': valid = this.validNIE(str); break;
            case 'cif': valid = this.validCIF(str); break;
            default: break;
        }

        return { type, valid };
    }

    documentType(str): string {
        if (str.match(this.DNI_REGEX)) {
            return 'dni';
        }
        if (str.match(this.CIF_REGEX)) {
            return 'cif';
        }
        if (str.match(this.NIE_REGEX)) {
            return 'nie';
        }
    }

    validDNI(dni): boolean {
        const dni_letters = 'TRWAGMYFPDXBNJZSQVHLCKE';
        const letter = dni_letters.charAt(parseInt(dni, 10) % 23);

        return letter === dni.charAt(8);
    }

    validNIE(nie): boolean {
        let niePrefix = nie.charAt(0);
        switch (niePrefix) {
            case 'X': niePrefix = 0; break;
            case 'Y': niePrefix = 1; break;
            case 'Z': niePrefix = 2; break;
            default: break;
        }

        return this.validDNI(niePrefix + nie.substr(1));
    }

    validCIF(cif): boolean {
        const match = cif.match(this.CIF_REGEX);
        const letter = match[1];
        const number = match[2];
        const control = match[3];
        let even_sum = 0;
        let odd_sum = 0;
        let n;

        for (let i = 0; i < number.length; i++) {
            n = parseInt(number[i], 10);
            if (i % 2 === 0) {
                n *= 2;
                odd_sum += n < 10 ? n : n - 9;
            } else {
                even_sum += n;
            }
        }

        const control_digit = (10 - (even_sum + odd_sum)).toString().substr(-1);
        const control_letter = 'JABCDEFGHI'.substr(Number(control_digit), 1);

        if (letter.match(/[ABEH]/)) {
            return control === control_digit;
        } else if (letter.match(/[KPQS]/)) {
            return control === control_letter;
        } else { return control === control_digit || control === control_letter; }

    }

}


```

