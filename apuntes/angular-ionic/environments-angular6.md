# Environments / Entornos
En angular 5 los entornos cuando lanzabas el serve, era ng serve --env=prod, pues bien, eso en Angular 6 a cambiado.
Ahora simplemente podriamos usar ng serve --prod y automaticamente cogeria el entorno de produccion (**environment.prod.ts**), pero tardaria bastante en ejecutarse, ya que primero tiene que minificar el codigo y hacer varios cambios internamente.

Vamos a crear un entorno que sea de produccion con el mismo archivo pero exactamente igual que cuando usamos el de desarrollo.

## 1. Creamos dos archivos

- src/environments/**environment.ts**
  - Entorno que usamos normalmente (en debug)
- src/environments/**environment.prod.ts**
  - Entorno para produccion


#### environment.ts
``` 
export const environment = {
  production: false,
  url: 'http://localhost:PUERTO/api/'
}; 
```


#### environment.prod.ts
``` 
export const environment = {
  production: true,
  url: 'http://TUWEB/api/'
}; 
```


## 2. Abrimos angular.json
### 2.1 Buscamos "configurations" y dentro veremos production, en el que nos encontramos con esto:

```
"production": {
  "optimization": true,
  "outputHashing": "all",
  "sourceMap": false,
  "extractCss": true,
  "namedChunks": false,
  "aot": true,
  "extractLicenses": true,
  "vendorChunk": false,
  "buildOptimizer": true,
  "fileReplacements": [
    {
      "replace": "src/environments/environment.ts",
      "with": "src/environments/environment.prod.ts"
    }
  ]
}
```

Basicamente vamos a duplicar el entorno pero cambiandole el nombre por prod, y eliminando acciones innecesarias.

```
"prod": {
  "fileReplacements": [
    {
      "replace": "src/environments/environment.ts",
      "with": "src/environments/environment.prod.ts"
    }
  ]
}
```

El unico cambio, es que hemos eliminado lineas que hacian que tardase mas la compilacion.

### 2.2 Buscamos "serve"
Actualmente tenemos en configurations un solo entorno, tenemos que añadir el recien creado, para ello duplicaremos las lineas de production y le cambiamos el nombre
```
 "serve": {
  "builder": "@angular-devkit/build-angular:dev-server",
  "options": {
    "browserTarget": "frontend:build"
  },
  "configurations": {
    "production": {
      "browserTarget": "frontend:build:production"
    },
  }
},
```

Ahora quedaria asi, con prod y apuntando el **browserTarget** a **frontend:build:prod**
```
"serve": {
      "builder": "@angular-devkit/build-angular:dev-server",
      "options": {
        "browserTarget": "frontend:build"
      },
      "configurations": {
        "production": {
          "browserTarget": "frontend:build:production"
        },
        "prod": {
          "browserTarget": "frontend:build:prod"
        }
      }
    },
```

Y finalmente para poder usar el entorno lo hariamos asi:
- ng serve -c prod

Podeis notar el cambio con el entorno de produccion por defecto poniendo: 
- ng serve -c production

---------

[Volver](./README.md)
