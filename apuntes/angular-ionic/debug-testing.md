# Depuracion en Karma
Para poder ejecutar el console log en los tests, en el archivo karma.conf.js

`
client: {
    captureConsole: true,
    mocha: {
        bail: true
    }
}
`

----------------------------

[Volver](./README.md)