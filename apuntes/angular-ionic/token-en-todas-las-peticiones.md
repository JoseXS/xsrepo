# Token en todas las peticiones HTTP

Para poder enviar en todas nuestras peticiones get, post, put o cual sea, un token de autenticacion, podemos seguir estos pasos.

### auth.interceptor.ts
Creamos el archivo interceptor que se encargara de crear las cabeceras automaticas en cada peticion. La parte de **Authorization** es donde añadiremos nuestro token, en este ejemplo, sacaria de la funcion getToken (que solo consulta si existe, en localstorage) de userProvider y la insertaria en cada cabecera.
```
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor() { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone({
            setHeaders: {
                'Content-Type': 'application/json; charset=utf-8',
                Accept: 'application/json',
                Authorization: `${this.userProvider.getToken()}`
            }
        });

        return next.handle(req);
    }
}
```

### app.module.ts
Añadimos a nuestro app.module.ts un interceptor que sera el que gestione las peticiones.
```
import { AuthInterceptor } from './providers/auth.interceptor';
@NgModule({
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ] 
})
```

-----

[Volver](./README.md)