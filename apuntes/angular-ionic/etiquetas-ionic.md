# Etiquetas y Atributos Ionic HTML
En Ionic tenemos multitud de atributos, para poder añadir al HTML, y asi obtener diversos comportamientos.

He creado unas tablas para que sea mas facil recordar que opciones tiene cada atributo

## Atributos Genericos
| **Comando**   | **Descripcion**                                                                                                                       |
| ------------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| **disabled**  | Se puede aplicar a la mayoría de los componentes de entrada para deshabilitar la interacción (también agrega un estilo deshabilitado) |
| **ion-fixed** | Agrega elemento al área de contenido fijo para que no se vea afectado por el desplazamiento                                           |


## ion-list
| **Comando**  | **Descripcion**                                                             |
| ------------ | --------------------------------------------------------------------------- |
| **no-lines** | Elimina fronteras de la lista                                               |
| **inset**    | Aplica el estilo de inserción a la lista (agrega margen en todos los lados) |


## ion-item
| **Comando**                             | **Descripcion**                                                                                                         |
| --------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| **detail-none, detail-push**            | Se pueden usar para ocultar o mostrar el icono de flecha en un elemento de la lista                                     |
| **item-left, item-right, item-content** | Estos atributos decidir dónde, en relación con otros elementos, que el tema debe ser colocado                           |
| **text-wrap**                           | Forzará el texto dentro del elemento para que se ajuste a la siguiente línea                                            |
| **sticky**                              | Causará que los divisores de elementos se adhieran a la parte superior de la pantalla cuando se desplacen por el pasado |


## ion-input
| **Comando**   | **Descripcion**                                 |
| ------------- | ----------------------------------------------- |
| **autofocus** | Automáticamente enfocará el elemento de entrada |


## ion-label
| **Comando**  | **Descripcion**                                                        |
| ------------ | ---------------------------------------------------------------------- |
| **fixed**    | Etiqueta de posiciones a la izquierda del elemento de entrada          |
| **floating** | Hace que la etiqueta flote y se contraiga cuando se ingresa la entrada |
| **stacked**  | Etiqueta de posiciones encima del elemento de entrada                  |


## button
| **Comando**               | **Descripcion**                                                                |
| ------------------------- | ------------------------------------------------------------------------------ |
| **ion-button**            | Aplica el estilo de botón Iónico al botón. Esto siempre debe estar incluido    |
| **icon-only**             | Debería usarse en botones que solo contienen un <ion-icon>                     |
| **icon-right, icon-left** | Posiciones ion-icon dentro del botón                                           |
| **full**                  | Hace que el botón de ancho completo                                            |
| **block, round**          | Estilos del botón como un botón de bloque o botón redondo                      |
| **outline**               | Estilos del botón con un borde de color en lugar de un color de relleno sólido |
| **large, small**          | Crea botones más pequeños o más grandes                                        |


## ion-content
| **Comando**                                                  | **Descripcion**                                                                   |
| ------------------------------------------------------------ | --------------------------------------------------------------------------------- |
| **padding, no-padding**                                      | Agrega o elimina el relleno del área de contenido                                 |
| **padding-left, padding-right, padding-top, padding-bottom** | Añade el relleno a un lado específico del área de contenido                       |
| **padding-horizontal, padding-vertical**                     | Aplica relleno a la izquierda y derecha, o arriba y abajo del área de contenido   |
| **margin**                                                   | Agrega un margen al área de contenido                                             |
| **margin-left, margin-right, margin-top, margin-bottom**     | Añade un margen a un lado específico del área de contenido                        |
| **margin-horizontal, margin-vertical**                       | Aplica un margen a la izquierda y derecha, o arriba y abajo del área de contenido |


## ion-col
| **Comando**                                 | **Descripcion**                                                                                                       |
| ------------------------------------------- | --------------------------------------------------------------------------------------------------------------------- |
| **width-10,20,25,33,34,50,66,67,75,80,90**  | Controla el ancho de una columna de cuadrícula                                                                        |
| **offset-10,20,25,33,34,50,66,67,75,80,90** | Compensa una columna por la cantidad especificada (agrega un margen izquierdo)                                        |
| **top, bottom, center**                     | se alinea verticalmente elementos en el interior de una columna a la parte superior, inferior, o centro de la columna |


## ion-row
| **Comando**                                     | **Descripcion**                                                                                                   |
| ----------------------------------------------- | ----------------------------------------------------------------------------------------------------------------- |
| **wrap**                                        | Causará que las columnas se envuelvan en la siguiente línea cuando no hay espacio                                 |
| **top, bottom, center**                         | se alinea verticalmente los elementos dentro de una fila a la parte superior, inferior, o en el centro de la fila |
| **responsive-sm, responsive-md, responsive-lg** | Hace que la cuadrícula sensible al cambiar la flex-directiona columnen un cierto punto de interrupción            |


## ion-header
| **Comando**     | **Descripcion**                              |
| --------------- | -------------------------------------------- |
| **no-border**   | elimina el borde del elemento del encabezado |
| **transparent** | agrega un fondo transparente al encabezado   |


## ion-buttons
| **Comando**          | **Descripcion**                                                      |
| -------------------- | -------------------------------------------------------------------- |
| **left, right, end** | posicionará los botones contenidos en el elemento de forma apropiada |


----------------------------

[Volver](./README.md)