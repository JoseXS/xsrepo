As a workaround you can edit

`node_modules/@ionic/app-scripts/dist/dev-server/serve-config.js`
replace

``exports.ANDROID_PLATFORM_PATH = path.join('platforms', 'android', 'assets', 'www');``
with

``exports.ANDROID_PLATFORM_PATH = path.join('platforms', 'android', 'app', 'src', 'main', 'assets', 'www');``

---

[Volver](../README.md)