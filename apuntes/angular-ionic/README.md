# Angular
> **Nota**: Disponible los enlaces de [Angular / Ionic](../enlaces/desarrollo.md#angular)

- [Actualizar Angular 5 a 6](actualizar-angular-5-a-6.md)
- [Environments Angular 6](environments-angular6.md)
- [Angular CLI](angular-cli.md)
- [Directiva Imagenes fallidas - (Image-preload)](directiva-imagenes-fallidas.md)
- [Token en todas las peticiones HTTP](token-en-todas-las-peticiones.md)
- [Servicio/Provider para validar documentos de identidad](validadordniniecif.md)
- [Comprobar tamaño de modulos instaldos](comprobar-tamano-modulos-instalados.md)

# Ionic
- [Etiquetas de Ionic](etiquetas-ionic.md)
- [Configurar APP](configurar-app.md)
- [Plugins Nativos en dispositivos](nativo-en-dispositivos.md)

# Karma / Jazmine
- [Primeros pasos](primeros-pasos-testing.md)
- [Console log en tests](debug-testing.md)

----------------------------

[Volver](../README.md)