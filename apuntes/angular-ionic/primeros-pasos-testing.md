# Primeros pasos

## Jasmine: 
- Jasmine es un framework para escribir pruebas unitarias, nos facilita herramientas para poder escribir pruebas de forma controlada y tiene funciones para hacer verificaciones, en este artículo Pruebas unitarias: Introducción a Jasmine hablamos más a profunidad sobre Jasmine.
## Karma: 
- Es el encargado de correr todas las pruebas unitarias que escribamos y como resultado nos da un reporte que nos muestra las pruebas que pasaron y las pruebas que fallaron. En la mayoría de veces para ejecutar las pruebas unitarias lanza un navegador para correr las pruebas unitarias.


## El mantra AAA (Arrange, Act, Assert)
Es un patrón que nos sugiere cómo debemos ordenar y escribir las pruebas, cada caso de prueba debería seguir el siguiente formato:
- Arrange: En esta parte de preparan todas precondiciones para el caso de prueba.
- Act: Se ejecuta el método o objeto el cual se quiere probar.
- Assert: Verificar que el resultado obtenido sea el esperado.
	
***Ejemplo:***
``` 
describe("Test for Math.pow", () => {
it('should return 25', () => {
    // Arrange
    const base, exponent = 5, 2;
    // Act
    const response = Math.pow(base, exponent);
    // Assert
    expect(response).toEqual(25); 
  });});
```
-----------

Github con ejemplos de testing: https://github.com/spektrakel-blog/ng-testing-snippets 

-----------

La API para las solicitudes de coincidencia se basa en tres métodos:
- expectOne(expr): espera exactamente una solicitud que coincida
- expectNone(expr): esperar que ninguna solicitud coincida
- match(expr): coincide con la solicitud pero no verifica / afirma
