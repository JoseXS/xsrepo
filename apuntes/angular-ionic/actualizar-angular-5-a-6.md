- En la carpeta del proyecto instalar Angular Cli local

`` npm install @angular/cli@latest ``

- Actualizar Angular CLI local y el proyecto con ng update

`` ng update @angular/cli ``

- Ver las librerias que necesitan actualización

`` ng update ``

- Actualizar librerias del proyecto

`` ng update --all --force ``

- Desinstalar y actualizar las dependencias del proyecto segun nos vaya diciendo el compilador

----------------------------

[Volver](./README.md)