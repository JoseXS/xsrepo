# Comandos utiles


## Resumen
1. [Informacion y supervision del sistema](#1-informacion-y-supervision-del-sistema)
   1. [Gestion de recursos](#11-gestion-de-recursos)
   2. [Espacio de disco](#12-espacio-de-disco)
   3. [Informacion del sistema](#13-informacion-del-sistema)
   4. [Apagado y recinicio del sistema](#14-apagado-y-recinicio-del-sistema)
   5. [Fechas](#15-fechas)
2. [Operaciones con archivos y directorios](#2-operaciones-con-archivos-y-directorios)
   1. [Desplazarse entre directorios](#21-desplazarse-entre-directorios)
   2. [Listar archivos y directorios](#22-listar-archivos-y-directorios)
   3. [Manipulacion de archivos y directorios](#23-manipulacion-de-archivos-y-directorios)
   4. [Enlaces simbolicos](#24-enlaces-simbolicos)
   5. [Codificacion](#25-codificacion)
3. [Operaciones de contenido de archivo](#3-operaciones-de-contenido-de-archivo)
    1. [Tuberias y redirecciones I/O](#31-tuberias-y-redirecciones-io)
    2. [Mostrar contenido de archivo](#32-mostrar-contenido-de-archivo)
    3. [Manipulacion de texto](#33-manipulacion-de-texto)
4. [Busqueda de archivos y en su contenido](#4-busqueda-de-archivos-y-en-su-contenido)
    1. [Buscador de archivos](#41-buscador-de-archivos)
5. [Gestion de usuarios y grupos](#5-gestion-de-usuarios-y-grupos)
    1. [Usuarios](#51-usuarios)
    2. [Grupos de usuarios](#52-grupos-de-usuarios)
    3. [Comprobaciones](#53-comprobaciones)
6. [Permisos y atributos especiales](#6-permisos-y-atributos-especiales)
    1. [Permisos de archivos y carpetas](#61-permisos-de-archivos-y-carpetas)
    2. [Permisos SUID](#62-permisos-suid)
    3. [Atributos especiales de archivo](#63-atributos-especiales-de-archivo)
7. Archivos empaquetados y comprimidos
    1. [Archivos TAR](#71-archivos-tar)
    2. [Archivos ZIP](#72-archivos-zip)
    3. [Archivos BZ2](#73-archivos-bz2)
    4. [Archivos GZ](#74-archivos-gz)
    5. [Archivos RAR](#75-archivos-rar)
8.  [Instaladores de paquetes y repositorios](#8-instaladores-de-paquetes-y-repositorios)
    1. [Paquetes DEB](#81-paquetes-deb)
    2. [Actualizador de paquetes APT](#82-actualizador-de-paquetes-apt)
9. [Operaciones con sistemas de archivos](#9-operaciones-con-sistemas-de-archivos)
    1. [Analisis del sitema de archivos](#91-analisis-del-sitema-de-archivos)
    2. [Formatear sistema de archivos](#92-formatear-sistema-de-archivos)
    3. [Montar sistemas de archivos](#93-montar-sistemas-de-archivos)
    4. [Imagenes ISO Y grabadores de CDROM](#94-imagenes-iso-y-grabadores-de-cdrom)
    5. [Trabajo con la SWAP](#95-trabajo-con-la-swap)
    6. [Copias de seguridad y backups](#96-copias-de-seguridad-y-backups)
10. [Telecomunicaciones y operaciones de red](#10-telecomunicaciones-y-operaciones-de-red)
    1. [Descarga archivos internet](101-descarga-archivos-internet)
    2. [Operaciones de red](#102-operaciones-de-red)
    3. [SSH, SCP Y Tunneling](#103-ssh-scp-y-tunneling)
    4. [Redes de Microsoft Windows (Samba)](#104-redes-de-microsoft-windows-samba)

----

## 1. Informacion y supervision del sistema
| Comando | Descripcion                   |
| ------- | ----------------------------- |
| clear   | Limpiar pantalla del terminal |
| reset   | Reiniciar sesion del terminal |
| exit    | Salir del terminal            |

----

### 1.1 Gestion de recursos
| Comando | Descripcion                                              |
| ------- | -------------------------------------------------------- |
| top     | Mostar tareas en ejecucion y su uso de recursos          |
| htop    | Mostar tareas en ejecucion y su uso de recursos mejorado |
| free -h | Mostrar el estado de la ram                              |

----

### 1.2 Espacio de disco
| Comando           | Descripcion                                                          |
| ----------------- | -------------------------------------------------------------------- |
| df -h             | Mostrar una lista de las particiones montadas                        |
| ls -lSr           | Mostrar el tamaño de los archivos y directorios ordenados por tamaño |
| du -sh directorio | Estimar el espacio usado por el directorio ‘dir1′                   |

----

### 1.3 Informacion del sistema
| Comando                          | Descripcion                                                      |
| -------------------------------- | ---------------------------------------------------------------- |
| who -a                           | Mostrar usuarios conectados                                      |
| last reboot                      | Mostrar historial de reinicio                                    |
| uname -a                         | Mostrar arquitectura y versión de Linux y Kernel                 |
| lsmod                            | Mostrar el kernel cargado.                                       |
| dmidecode -q                     | Mostrar componentes de hardware del sistema                      |
| cat /etc/fstab                   | Mostrar particiones de disco duro                                |
| hdparm -i /dev/hda               | Mostrar características de disco duro                            |
| lspci                            | Mostrar dispositivos PCI                                         |
| lsusb                            | Mostrar dispositivos USB                                         |
| tail /var/log/dmesg              | Mostrar eventosde proceso de carga de kernel                     |
| tail /var/log/messages           | Mostrar los eventos del sistema                                  |
| lsof -p $$                       | Mostrar lista de archivos abiertos por procesos                  |
| lsof /directorio                 | Mostrar lista de archivos abiertos en un camino dado del sistema |
| strace -c ls >/dev/null          | Mostrar llamadas del sistema hechas y recibidas por un proceso   |
| strace -f -e open ls >/dev/null  | Mostrar las llamadas a la biblioteca                             |
| watch -n1 'cat /proc/interrupts' | Mostrar interrupciones en tiempo real                            |

----

### 1.4 Apagado y recinicio del sistema
| Comando                  | Descripcion                 |
| ------------------------ | --------------------------- |
| logout                   | Cerrar sesion usuario       |
| shutdown now             | Apagar el sistema ahora     |
| shutdown -r now          | Reiniciar sistema ahora     |
| shutdown horas:minutos & | Apagado programado          |
| shutdown -c              | Cancelar apagado programado |

----

### 1.5 Fechas

| Comando          | Descripcion                     |
| ---------------- | ------------------------------- |
| date             | Mostrar la fecha del sistema    |
| cal año          | Mostrar calendario de un año    |
| cal mes año 2018 | Mostrar calendario de mes y año |

---

## 2. Operaciones con archivos y directorios
### 2.1 Desplazarse entre directorios

| Comando                    | Descripcion                          |
| -------------------------- | ------------------------------------ |
| cd                         | Ir a directorio de raiz              |
| cd ..                      | Ir a directorio anterior             |
| cd /directorio1/directorio | Entrar en directorio (ruta absoluta) |
| cd directorio1/directorio2 | Entrar en directorio (ruta relativa) |
| cd ~                       | Ir a directorio de usuario           |
| cd -                       | Ir a ultimo directorio visitado      |
| pwd                        | Mostrar ruta actual                  |


--- 
### 2.2 Listar archivos y directorios
| Comando | Descripcion                                          |
| ------- | ---------------------------------------------------- |
| ls      | Mostrar archivos y directorios                       |
| ls -l   | Mostrar archivos y directorios con detalles          |
| ls -la  | Mostrar archivos y directorios incluidos los ocultos |

--- 
### 2.3 Manipulacion de archivos y directorios
| Comando                           | Descripcion                                              |
| --------------------------------- | -------------------------------------------------------- |
| mv origen destino                 | Renombrar o mover un archivo o directorio                |
| cp archivo direccion              | Copiar un archivo                                        |
| cp -r origen destino              | Copiar un directorio                                     |
| rm archivo                        | Borrar el archivo llamado archivo                        |
| rm -d directorio                  | Borrar directorio si está vacio                          |
| rm -r directorio                  | Borrar directorio y su contenido                         |
| mkdir directorio                  | Crear nuevo directorio                                   |
| mkdir directorio1 directorio2     | Crear varios directorios simultáneamente                 |
| mkdir -p /directorio1/directorio2 | Crear ruta de directorios                                |
| touch archivo                     | Crear archivo vacio                                      |
| touch -t 19901230000 archivo      | Cambiar fecha de archivo (formato año, mes, dia, y hora) |

--- 
### 2.4 Enlaces simbolicos
| Comando            | Descripcion                                       |
| ------------------ | ------------------------------------------------- |
| ln -s archivo lnk1 | Crear un enlace simbólico al archivo o directorio |
| ln archivo lnk1    | Crear enlace físico al archivo o directorio       |

--- 
### 2.5 Codificacion
| Comando         | Descripcion                            |
| --------------- | -------------------------------------- |
| md5sum archivo  | Calcular md5 de un archivo             |
| gpg -c          | Codificar archivo GNU Privacy Guard.   |
| gpg archivo.gpg | Decodificar archivo GNU Privacy Guard. |

--- 
## 3. Operaciones de contenido de archivo

### 3.1 Tuberias y redirecciones I/O
| Comando                    | Descripcion                                            |
| -------------------------- | ------------------------------------------------------ |
| comando > archivo_out.txt  | Dirigir salida de comando a nuevo archivo              |
| comando >> archivo_out.txt | Digir salida de comando para añadir a archivo archivo  |
| comando < archivo_in.txt   | Dirigir entrada de comandos                            |
| comando &> archivo_out.txt | Dirigir salida estandar y salida de error a un fichero |
 
#

--- 
### 3.2 Mostrar contenido de archivo
| Comando         | Descripcion                                                        |
| --------------- | ------------------------------------------------------------------ |
| echo archivo    | Muestra contenido de archivo                                       |
| cat archivo     | Mostrar contenido de archivo                                       |
| tac archivo     | Mostrar contenido de archivo empezando por el final                |
| more archivo    | Mostrar contenido de archivo desplazandose linea a linea           |
| less archivo    | Mostrar contenido de archivo desplazandose adelante o atrás        |
| head -2 archivo | Mostrar dos primeras lineas de archivo                             |
| tail -2 archivo | Mostrar dos ultimas lineas de archivo                              |
| tail -f archivo | Mostrar en tiempo real las ultimas lineas de archivo (seguimiento) |

--- 

### 3.3 Manipulacion de texto
| Comando                           | Descripcion                                           |
| --------------------------------- | ----------------------------------------------------- |
| sed '3,5d' archivo                | Eliminar lineas 1 a 5 de archivo                      |
| sed '5,$d' archivo                | Eliminar lineas 5 a fin de archivo                    |
| sed '/^$/d' archivo               | Eliminar linas en blanco                              |
| sed '/ *#/d; /^$/d' archivo       | Eliminar linas en blanco y comentarios                |
| sed 's/cadena1/cadena2/g' archivo | Sustituir una cadena por otra                         |
| sed -n '/cadena/p'                | Visualizar unicamente las líneas que contienen cadena |

---

## 4. Busqueda de archivos y en su contenido

### 4.1 Buscador de archivos
| Comando                                          | Descripcion                                                                  |
| ------------------------------------------------ | ---------------------------------------------------------------------------- |
| find * -name nombre                              | Buscar archivo y directorio por su nombre en todo el sistema                 |
| find directorio -name nombre≥                    | Buscar archivo y directorio por su nombre, dentro de directorio              |
| find directorio -user usuario                    | Buscar archivos y directorios pertenecientes a usuario, dentro de directorio |
| find directorio -type f tipo                     | Buscar archivos y directorios por su tipo, dentro de directorio              |
| find directorio -name nombre -exec comando {} \; | Buscar archivos y ejecutar comando                                           |
| locate \*.ps                                     | Buscar archivos con extensión .ps                                            |
| which ejecutable                                 | Mostrar la ruta completa de un ejecutable                                    |
| whereis ejecutable                               | Mostrar la ubicación de un archivo binario, de ayuda o fuente                |

---

## 5. Gestion de usuarios y grupos
### 5.1 Usuarios
| Comando                                                                    | Descripcion                                               |
| -------------------------------------------------------------------------- | --------------------------------------------------------- |
| useradd usuario                                                            | Crear un nuevo usuario                                    |
| useradd -c "Nombre Usuario" -g grupo -d /home/usuario -s /bin/bash usuario | Crear usuario, version completa                           |
| userdel usuario                                                            | Borrar usuario                                            |
| userdel -r usuario                                                         | Borrar usuario y eliminar su directorio home              |
| usermod -c "Nombre usuario" -g grupo -d /home/usuario -s /bin/bash usuario | Cambiar los atributos del usuario.                        |
| passwd                                                                     | Cambiar contraseña del propio usuario.                    |
| passwd usuario                                                             | Cambiar la contraseña de usuario                          |
| chage -E 2014-12-31 usuario                                                | Colocar un fecha de expiracion para contraseña de usuario |

--- 

### 5.2 Grupos de usuarios
| Comando                                  | Descripcion                                                           |
| ---------------------------------------- | --------------------------------------------------------------------- |
| groupadd nombre_grupo                    | Crear grupo usuarios                                                  |
| groupdel nombre_grupo                    | Borrar grupo de usuarios                                              |
| groupmod -n nombre_nuevo nombre_anterior | Renombrar grupo de usuarios                                           |
| newgrp grupo                             | Modificar el grupo actual de un usuario que pertenece a varios grupos |
| groups                                   | Listar grupos del usuario actual                                      |
| cut -d: -f1 /etc/group                   | Listar todos los grupos                                               |

--- 

### 5.3 Comprobaciones
| Comando | Descripcion                                                     |
| ------- | --------------------------------------------------------------- |
| pwck    | Comprueba que el archivo de contraseñas /etc/passwd es correcto |
| grpck   | Comprueba que el archivo de grupos /etc/group #es correcto      |

--- 

## 6. Permisos y atributos especiales
### 6.1 Permisos de archivos y carpetas
| Comando                     | Descripcion                                                                                                            |
| --------------------------- | ---------------------------------------------------------------------------------------------------------------------- |
| ls -lh                      | Mostrar permisos.                                                                                                      |
| chmod 0777 fichero          | Asignar permisos 0777 a fichero                                                                                        |
| chmod -R 0644 directorio    | Asignar permisos a todos los archivos de un directorio                                                                 |
| chmod ugo+rwx directorio    | Colocar a directorio permisos de lectura (r), escritura (w) y ejecución (x) al propietario (u), grupo (g) y otros (o). |
| chown usuario archivo       | Emplear las opciones necesarias para añadir o quitar los permisos deseados                                             |
| chown -R usuario directorio | Cambiar usuario a todos los archivos de un directorio                                                                  |
| chgrp grupo archivo         | Cambiar grupo de archivo                                                                                               |
| chown usuario:grupo archivo | Cambiar usuario y grupo de archivo.                                                                                    |

--- 

### 6.2 Permisos SUID
| Comando                    | Descripcion                                                                                                         |
| -------------------------- | ------------------------------------------------------------------------------------------------------------------- |
| find / -perm -u+s          | Visualizar todos los archivos del sistema con SUID configurado                                                      |
| chmod u+s /bin/archivo     | Colocar bit SUID en archivo binario. El usuario que ejecute este archivo adquiere los mismos privilegios como dueño |
| chmod u-s /bin/archivo     | Eliminar bit SUID en archivo binario                                                                                |
| chmod g+s /home/directorio | Colocar bit SGID en directorio. Similar a SUID pero para directorios                                                |
| chmod g-s /home/directorio | Eliminar bit SUID en archivo binario.                                                                               |
| chmod o+t /home/directorio | Colocar un bit STIKY en un directorio. Permite el borrado de archivos solamente a los dueños legítimos              |
| chmod o-t /home/directorio | Eliminar bit STIKY en un directorio.                                                                                |

--- 
### 6.3 Atributos especiales de archivo
| Comando           | Descripcion                                                                                               |
| ----------------- | --------------------------------------------------------------------------------------------------------- |
| lsattr            | Mostrar atributos especiales.                                                                             |
| chattr +a archivo | Permite escribir abriendo un archivo solamente modo append.                                               |
| chattr +c archivo | Permite que un archivo sea comprimido / descomprimido automaticamente.                                    |
| chattr +d archivo | Asegura que el programa ignore borrar los archivos durante la copia de seguridad.                         |
| chattr +i archivo | Convierte el archivo en invariable, por lo que no puede ser eliminado, alterado, renombrado, ni enlazado. |
| chattr +s archivo | Permite que un archivo sea borrado de forma segura.                                                       |
| chattr +S archivo | Asegura que un archivo sea modificado, los cambios son escritos en modo synchronous como con sync.        |
| chattr +u archivo | Te permite recuperar el contenido de un archivo aún si este está cancelado.                               |

--- 

## 7. Archivos empaquetados y comprimidos
### 7.1 Archivos TAR
| Comando                                            | Descripcion                                                   |
| -------------------------------------------------- | ------------------------------------------------------------- |
| tar -tf archive.tar                                | Mostrar contenido de archivo tar                              |
| tar -cvf archivo.tar directorio1                   | Crear archivo tar                                             |
| tar -cvf archivo.tar archivo1 archivo2 directorio1 | Crear archivo tar compuesto por varios archivos y directorios |
| tar -cvfj archivo.tar.bz2                          | Crear archivo tar comprimido en bzip2                         |
| tar -cvfz archivo.tar.gz directorio1               | Crear archivo tar comprimido en gzip                          |
| tar -xvf archivo.tar                               | Extraer archivo tar                                           |
| tar -xvf archivo.tar -C /directorio                | Extraer archivo tar en directorio                             |
| tar -xpvzf archivo.tar                             | Extraer archivo tar preservando permisos de usuario           |
| tar -xvfj archivo.tar.bz2                          | Descomprimir archivo tar comprimido en bzip2                  |
| tar -xvfz archivo.tar.gz                           | Descomprimir archivo tar comprimido en gzip                   |

---

### 7.2 Archivos ZIP
| Comando                                          | Descripcion                                                  |
| ------------------------------------------------ | ------------------------------------------------------------ |
| zip archivo.zip directorio1                      | Crear un archivo comprimido en zip.                          |
| zip -r archivo.zip archivo1 archivo2 directorio1 | Comprimir en zip compuesto por varios archivos y directorios |
| unzip archivo.zip                                | Descomprimir un archivo zip                                  |

---

### 7.4 Archivos BZ2
| Comando             | Descripcion              |
| ------------------- | ------------------------ |
| bzip2 archivo       | Comprimir archivo bz2    |
| bunzip2 archivo.bz2 | Descomprimir archivo bz2 |

---

### 7.5 Archivos GZ
| Comando           | Descripcion                                   |
| ----------------- | --------------------------------------------- |
| gzip archivo      | Comprimir archivo gz                          |
| gunzip archivo.gz | Descomprimir archivo gz                       |
| gzip -9 archivo   | Descomprimir archivo gz con compresion maxima |

---

### 7.6 Archivos RAR
| Comando                                         | Descripcion                                                   |
| ----------------------------------------------- | ------------------------------------------------------------- |
| rar a archivo.rar directorio1                   | Crear archivo rar                                             |
| rar a archivo.rar archivo1 archivo2 directorio1 | Crear archivo zip compuesto por varios archivos y directorios |
| unrar x archivo.rar                             | Descomprimir archivo rar.                                     |

---- 

## 8. Instaladores de paquetes y repositorios
| Comando                                            | Descripcion                                           |
| -------------------------------------------------- | ----------------------------------------------------- |
| ldd programa                                       | Mostrar bibliotecas requeridas por programa o comando |
| git clone git://github.com/directorio/proyecto.git | Descargar de Github                                   |

----

### 8.1 Paquetes DEB
| Comando                    | Descripcion                                                           |
| -------------------------- | --------------------------------------------------------------------- |
| dpkg -i paquete.deb        | Instalar / actualizar un paquete deb                                  |
| dpkg -r paquete            | Eliminar un paquete deb del sistema                                   |
| dpkg -l                    | Mostrar todos los paquetes deb instalados en el sistema               |
| dpkg -s paquete            | Obtener información en un paquete instalado en el sistema             |
| dpkg -L paquete            | Mostar lista de archivos dados por un paquete instalado en el sistema |
| dpkg –contents paquete.deb | Mostrar lista de archivos dados por un paquete sin instalar           |
| dpkg -S archivo            | Verificar a que paquete pertenece a un archivo                        |

---

### 8.2 Actualizador de paquetes APT
| Comando                                                | Descripcion                              |
| ------------------------------------------------------ | ---------------------------------------- |
| apt-get install paquete                                | Instalar / actualizar un paquete deb.    |
| sudo sh -c 'echo repositorio' >> /etc/apt/sources.list | Añadir repositorio                       |
| apt-get update                                         | Actualizar lista de paquetes             |
| apt-get upgrade                                        | Actualizar paquetes instalados           |
| apt-get remove paquete                                 | Eliminar un paquete del sistema          |
| apt-cache search paquete                               | Buscar un paquete                        |
| apt-get check                                          | verificar resolución de dependencias     |
| apt-get clean                                          | limpiar cache desde paquetes descargados |

----

## 9 Operaciones con sistemas de archivos

### 9.1 Analisis del sitema de archivos
| Comando                | Descripcion                                                        |
| ---------------------- | ------------------------------------------------------------------ |
| fsck /dev/hda1         | Comprobar integridad de archivos en el disco hda1 en sistema Linux |
| fsck.ext2 /dev/hda1    | Comprobar integridad de archivos en el disco hda1 en sistema ext2  |
| fsck.ext3 /dev/hda1    | Comprobar integridad de archivos en el disco hda1 en sistema ext3  |
| fsck.vfat /dev/hda1    | Comprobar integridad de archivos en el disco hda1 en sistema Fat   |
| fsck.msdos /dev/hda1   | Comprobar integridad de archivos en el disco hda1 en sistema Dos   |
| badblocks -v /dev/hda1 | Comprobar bloques defectuosos en el disco hda1.                    |

---

### 9.2 Formatear sistema de archivos
| Comando                      | Descripcion                    |
| ---------------------------- | ------------------------------ |
| mkfs /dev/hda1               | Formatear hda en sistema Linux |
| mkfs -t vfat 32 -F /dev/hda1 | Formatear hda en sistema FAT32 |
| mke2fs /dev/hda1             | Formatear hda en sistema ext2  |
| mke2fs -j /dev/hda1          | Formatear hda en sistema ext3  |

---

### 9.3 Montar sistemas de archivos
| Comando                           | Descripcion                                               |
| --------------------------------- | --------------------------------------------------------- |
| mount /dev/hda2 /mnt/hda2         | Montar un disco duro hda2                                 |
| mount /dev/fd0 /mnt/floppy        | Montar un disquetera                                      |
| mount /dev/cdrom /mnt/cdrom       | Montar un cdrom o dvdrom                                  |
| mount /dev/hdc /mnt/cdrecorder    | Montar cd regrabable dvdrom                               |
| mount /dev/hdb /mnt/cdrecorder    | Montar un cd o dvd regrabable                             |
| mount /dev/sda1 /mnt/usbdisk      | Montar un usb pen-drive o una memoria                     |
| mount -o loop file.iso /mnt/cdrom | Montar un archivo o una imagen iso                        |
| umount /dev/hda2                  | Desmontar un dispositivo llamado hda2                     |
| fuser -km /mnt/hda2               | Forzar el desmontaje (cuando el dispositivo está ocupado) |

---

### 9.4 Imagenes ISO Y grabadores de CDROM
| Comando                                                                            | Descripcion                        |
| ---------------------------------------------------------------------------------- | ---------------------------------- |
| mount -o loop cd.iso /mnt/iso                                                      | Montar una imagen iso              |
| mkisofs /dev/cdrom > cd.iso                                                        | Crear imagen iso de cdrom en disco |
| mkisofs -J -allow-leading-dots -R -V “Label CD” -iso-level 4 -o ./cd.iso data_cd | Crear imagen iso de un directorio  |
| cdrecord -v dev=/dev/cdrom cd.iso                                                  | Grabar imagen iso en cdrom         |
| cdrecord -v gracetime=2 dev=/dev/cdrom -eject blank=fast -force                    | Limpiar o borrar un cd regrabable  |

---

### 9.5 Trabajo con la SWAP
| Comando          | Descripcion                           |
| ---------------- | ------------------------------------- |
| mkswap /dev/hda3 | Crear archivo de sistema swap en hda3 |
| swapon /dev/hda3 | Activar particion swap en hda3        |

---

### 9.6 Copias de seguridad y backups
| Comando                                         | Descripcion                               |
| ----------------------------------------------- | ----------------------------------------- |
| dump -0aj -f /tmp/archivo.bak /directorio       | Hacer un backup completo de directorio    |
| dump -1aj -f /tmp/archivo.bak /directorio       | Realizar backup incremental de directorio |
| restore -if /tmp/archivo.bak                    | Restaurar un backup iterativamente        |
| rsync -rogpav –delete /directorio1 /directorio2 | Sincronizar directorios                   |
| dd if=/dev/sda of=/tmp/archivo                  | Volcar contenido de disco duro a archivo  |

``find /home/usuario -name '*.txt' | xargs cp -av –target-directory=/home/backup/ –parents ``
 Encontrar y copiar todos los archivos con extensión .txt de un directorio a otro 


``find /var/log -name '*.log' | tar cv –files-from=- | bzip2 > log.tar.bz2 ``
Encontrar todos los archivos con extensión .log y hacer un archivo bzip

---

## 10. Telecomunicaciones y operaciones de red
### 10.1 Descarga archivos internet
| Comando                           | Descripcion                                                                         |
| --------------------------------- | ----------------------------------------------------------------------------------- |
| wget www.paginaweb.com/archivo    | Descargar archivo desde paginaweb                                                   |
| wget -c www.paginaweb.com/archivo | Descargar un archivo con la posibilidad de parar la descargar y reanudar más tarde. |
| wget -r www.paginaweb.com         | Descargar paginaweb completa                                                        |

---

### 10.2 Operaciones de red
| Comando                                                       | Descripcion                                                                   |
| ------------------------------------------------------------- | ----------------------------------------------------------------------------- |
| ifconfig eth0                                                 | Mostrar la configuración de Ethernet                                          |
| ifup eth0                                                     | Activar interface eth0                                                        |
| ifdown eth0                                                   | Deshabilitar interface eth0                                                   |
| ifconfig eth0 192.168.1.1 netmask 255.255.255.0≤              | Configurar una dirección IP                                                   |
| ifconfig eth0 promisc                                         | Configurar eth0 en modo común para capturar paquetes (sniffing)               |
| dhclient eth0                                                 | Activar la interface ‘eth0′ en modo dhcp                                     |
| route -n                                                      | Mostrar mesa de recorrido                                                     |
| route add -net 0/0 gw IP_Gateway                              | Configurar entrada predeterminada                                             |
| route add -net 192.168.0.0 netmask 255.255.0.0 gw 192.168.1.1 | Configurar ruta estática para buscar la red ’192.168.0.0/16′                 |
| route del 0/0 gw IP_gateway                                   | Eliminar ruta estática                                                        |
| echo “1” > /proc/sys/net/ipv4/ip_forward                    | Activar recorrido ip                                                          |
| hostname                                                      | Mostrar nombre del host                                                       |
| host www.paginaweb.com                                        | Buscar nombre del host para resolver el nombre a una dirección ip             |
| nslookup www.paginaweb.com                                    | Buscar nombre del host para resolver el nombre a una dirección ip y viceversa |
| ip link show                                                  | Mostrar estado de enlace de todas las interfaces                              |
| mii-tool eth0                                                 | Mostrar estado de enlace de eth0                                              |
| ethtool eth0                                                  | Mostrar estadísticas de tarjeta de red eth0                                   |
| netstat -tup                                                  | Mostrar todas las conexiones de red activas y sus PID                         |
| netstat -tupl                                                 | Mostrar todos los servicios de escucha de red en el sistema y sus PID         |
| tcpdump tcp port 80                                           | Mostrar todo el tráfico HTTP                                                  |
| iwlist scan                                                   | Mostrar redes inalámbricas                                                    |
| iwconfig wlan0                                                | Mostrar configuración de una tarjeta de red inalámbrica                       |
| whois www.paginaweb.com                                       | Buscar en base de datos Whois                                                 |


---

### 10.3 SSH, SCP Y Tunneling
| Comando                                                              | Descripcion                                                                  |
| -------------------------------------------------------------------- | ---------------------------------------------------------------------------- |
| ssh usuario@servidor.dominio.es                                      | Iniciar sesion ssh                                                           |
| ssh -X usuario@maquina                                               | Iniciar sesion ssh con compatibilidad X11 (permite ejecutar tareas visuales) |
| ssh -p 15000 usuario@maquina                                         | Iniciar sesion ssh en puerto determinado                                     |
| scp /archivo usuario@servidor.dominio.es:/directorio≤                | Copiar archivo mediante scp                                                  |
| ssh -f usuario@servidor.dominio.es -L 2000:servidor.dominio.es:25 -N | Creacion de tunel ssh                                                        |
| ssh -v -L4001:localhost:4001 usuario@servidor.dominio.es             | Redireccion de puertos mediante tunneling                                    |


### 10.4 Redes de Microsoft Windows (Samba)
| Comando                       | Descripcion                                    |
| ----------------------------- | ---------------------------------------------- |
| nbtscan ip_addr               | Resolución de nombre de red bios               |
| nmblookup -A ip_addr          | Resolución de nombre de red bios               |
| smbclient -L ip_addr/hostname | Mostrar acciones remotas de un host en windows |


----------------------------

[Volver](./README.md)