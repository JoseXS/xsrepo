# Permisos

| **Comando**                                | **Descripcion**                                         |
| ------------------------------------------ | ------------------------------------------------------- |
| sudo chown -R josexs:josexs /server-utils/ | Hacer propietario a usuario sobre carpeta y subcarpetas |


----------------------------

[Volver](./README.md)