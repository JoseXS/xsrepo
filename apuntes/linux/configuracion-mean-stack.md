> Nota: Se ha utilizado un droplet con Ubuntu 16 de DigitalOcean

1. Cambiamos clave de usuario root
    - passwd
2. Añadimos usuario y lo añadimos al grupo sudo
    - adduser josexs
    - usermod -aG sudo josexs
    - Entramos con el nuevo usuario
3. Instalamos Git
   - sudo apt install git
   - git --version
4. Instalamos NodeJS
    - curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    - sudo apt install -y nodejs
    - node --version
5. Clonar repositorio
    - cd ~
    - mkdir apps
    - git clone https://github.com/jlengstorf/tutorial-deploy-nodejs-ssl-digitalocean-app.git app.example.com
6. Instalamos PM2
    - sudo npm install -g pm2
    cd ~/apps/app.example.com
    pm2 start app
7. Arranque automatico PM2
    - pm2 startup systemd
8. Instalación Letsencryptpm2 
Nota: Apuntar dominio hacia la ip del servidor antes de seguir
    - Sudo apt install bc
    - Vemos si apunta hacia la ip dig +short tudominio.com
9. Generamos certificado SSL
Primero ahi que parar el servicio Nginx
    - cd  /opt/letsencrypt
    - ./certbot-auto certonly --standalone
10. Renovacion automatica SSL
    - sudo crontab -e
    - 00 1 * * 1 /opt/letsencrypt/certbot-auto renew >> /var/log/letsencrypt-renewal.log
30 1 * * 1 /bin/systemctl reload nginx
11. Instalacion Nginx
    - sudo apt install nginx
12. Todo el trafico por Https
    - sudo nano /etc/nginx/sites-enabled/default
    - Eliminar todo y poner:
  ```
server {
    listen 80;
    listen [::]:80 default_server ipv6only=on;
    return 301 https://$host$request_uri;
}
```

13.  Crear grupo Diffie-Hellman seguro
    - sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
    - sudo nano /etc/nginx/snippets/ssl-params.conf
    - Copiar dentro:

```
# See https://cipherli.st/ for details on this configuration
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off; # Requires nginx >= 1.5.9
ssl_stapling on; # Requires nginx >= 1.3.7
ssl_stapling_verify on; # Requires nginx => 1.3.7
resolver 8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout 5s;
add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;
    # Add our strong Diffie-Hellman group
ssl_dhparam /etc/ssl/certs/dhparam.pem;
```

14. Configurar dominio para SSL
    - sudo nano /etc/nginx/sites-enabled/default
    - Añadimos acceso directo en carpeta sites-enables: sudo ln -s /etc/nginx/sites-available/dating.josexs.com /etc/nginx/sites-enabled/
    - Debajo de lo que ya hemos añadido antes, pegamos:

```
# HTTPS — proxy all requests to the Node app
server {

listen 443 ssl http2;
listen [::]:443 ssl http2;
server_name app.example.com;
        # Use the Let’s Encrypt certificates
ssl_certificate /etc/letsencrypt/live/app.example.com/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/app.example.com/privkey.pem;
        # Include the SSL configuration from cipherli.st
include snippets/ssl-params.conf;
        location / {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-NginX-Proxy true;
    proxy_pass http://localhost:5000/;
    proxy_ssl_session_reuse off;
    proxy_set_header Host $http_host;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
}
}
```
        
15. Comprobamos la configuración Nginx
    - sudo nginx -t
16. Habilitamos Nginx
    - sudo systemctl start nginx
    - Si no funciona correctamente, reiniciamos nginx
        - sudo systemctl restart nginx
        

### Siguientes pasos:
- Configurar varios dominios
- Configurar la url de la api como api.xsfiestas.es
- Subir git del proyecto ya con https
- Probar

> Si se necesitan añadir archivos grandes, en /etc/nginx/nginx.conf
dentro de http añadimos client_max_body_size 50M;

Archivo configuración final de dominio Nginx (Default)

```
server {
    listen 80;
    listen [::]:80;
    server_name josexs.com www.josexs.com;
    return 301 https://www.josexs.com$request_uri;
}

server {
        server_name josexs.com www.josexs.com;
        listen 443 ssl http2;
        listen [::]:443 ssl http2;

        ssl_certificate /etc/letsencrypt/live/josexs.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/josexs.com/privkey.pem;
        include snippets/ssl-params.conf;

        #Seguridad
        #location ~ (?:^|/)\. {
                        #deny all;
        #}

        #Esta parte la agregue por que no siempre el express mandaba todos los static files en mis apps

        #location ~* ^.+\.(jpg|jpeg|gif|png|ico|css|zip|tgz|gz|rar|bz2|pdf|txt|tar|wav|bmp|rtf|js|flv|swf|html|htm)$ {
                #root /home/josexs/apps/josexs/client/;
                #try_files $uri $uri/ /index.html;
        #}

        #Aqui se manda a que express se encargue de todo de back y servir front

        location / {
        root   /home/josexs/apps/josexs/;

        proxy_pass         http://127.0.0.1:3789;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-NginX-Proxy true;
        proxy_ssl_session_reuse off;
        proxy_set_header Host $http_host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
        }
}
```
Archivos configuracion:
Nginx: /etc/nginx/nginx.conf

----------------------------

[Volver](./README.md)