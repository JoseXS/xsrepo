# Configurar NodeJS sin sudo

- Haga un directorio para instalaciones globales:
  - mkdir ~/.npm-global
- Configure npm para usar la nueva ruta del directorio:
  - npm config set prefix '~/.npm-global'
- Abra o cree un ~/.profilearchivo y agregue esta línea:
  - export PATH=~/.npm-global/bin:$PATH
- De vuelta en la línea de comando, actualice las variables de su sistema:
  - source ~/.profile
  
----------------------------

[Volver](./README.md)