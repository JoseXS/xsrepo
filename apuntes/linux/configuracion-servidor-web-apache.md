# Configuracion servidor web con Apache

- Comprobar la ip que nos han facilitado esté limpia
  - Desde http://mxtoolbox.com/ introducimos la IP y nos dirá si tiene algún problema.
- Comprobamos el sistema operativo que nos han instalado
  - uname –a
- Actualizamos sistema y paquetes
  - sudo apt-get update
  - sudo apt-get upgrade
  - sudo apt-get dist-upgrade   
- Configurar zona horaria
  - rdpkg-reconfigure tzdata
- Instalamos SSH
  - sudo apt-get install openssh-server
- A partir de aqui podemos seguir desde el servidor o conectarnos por ssh
  - Ejemplo: ssh root@192.168.1.1
- Instalamos Htop para comprobación de recursos
  - sudo apt-get install htop
- Creamos usuario Nuevo
  - sudo adduser nombreusuario
- Permisos para el nuevo usuario
  - sudo usermod -aG sudo,adm usuario
> Nota: En /etc/sudoers podemos encontrar los usuarios con sus respectivos permisos
  > nombreusuario    ALL=(ALL:ALL) ALL

- Instalamos Apache, PHP, Mysql y PHPMyadmin
> Nota: podemos instalar todos los paquetes seguidos de un espacio entre cada uno
- sudo apt-get install apache2
- sudo apt-get install mysql-server mysql-client
- sudo apt-get install php7.0 php7.0-mysql php7.0-curl php7.0-json php7.0-cgi libapache2-mod-php7.0 php-mbstring php7.0-mbstring php-gettext
- sudo apt-get install phpmyadmin
- Reiniciamos servidor Apache
  - sudo service apache2 restart
- Instalacion FTP
  - sudo apt-get install ftpd
- Ver Configuracion FTP
  - cat /etc/vsftpd.conf | grep -v “^#”

## Completar instalacion
- Hacemos escribible la carpeta para la creacion del archivo de configuracion
  - sudo chmod -R 777 /var/www/html/moodle
- Entramos desde el navegador a http://ip/moodle y seguimos los pasos
- Cambiar la ruta de moodledata a /var/moodledata
- Una vez terminada la instalacion volvemos a dejar los permisos de la carpeta moodle a 775
  - sudo chmod -R 775 /var/www/html/moodle

## Seguridad
#### Phpmyadmin no accesible por la ruta por defecto
- Editamos el archivo /etc/phpmyadmin/apache.conf
- Buscamos -> Alias /phpmyadmin /usr/share/phpmyadmin
- Modificamos por -> Alias /nombrequequeramos /usr/share/phpmyadmin
- Reiniciamos apache y comprobamos

#### No mostrar la version de Linux que estamos usando
- Editamos el archivo /etc/apache2/apache2.conf
- Añadimos las siguientes lineas (Al final del archivo, por ejemplo):
  - ServerSignature Off
  - ServerTokens Prod
- Reiniciamos apache
 
**Notas**
- El usuario de MYSQL que hemos creado, solo tiene permisos para la base de datos recien creada
- Archivos Logs de Apache: nano /var/log/apache2/error.log
- Si no se puede conectar por clientes a mysql
  - sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf
  - Buscamos bind-address y comentamos con #
  - Reiniciamos mysql - sudo /etc/init.d/mysql restart
 
## Propietarios de archivos
> Esto nos sirve para que el grupo www-data sea propietario del archivo al igual que nuestro usuario. Tambien para que cada archivo que subamos no tengamos que darle permiso, para mas seguridad no es necesario añadir esto
  - sudo chown www-data:www-data -R /var/www/
  - sudo chown -R xskunk:www-data /var/www/
  - sudo usermod -a -G www-data xskunk
  - sudo chmod g+s /var/www/html/
  - find /var/www/ * -type d -print0 | sudo xargs -0 chmod g+s

## Firewall
- Comprobamos estado del Firewall
  - sudo ufw status
- Habilitamos Firewall
  - sudo ufw enable
- Abrir Puerto
  - sudo ufw allow 443
- Cerrar Puerto
  - sudo ufw deny

-----------

[Volver](./README.md)