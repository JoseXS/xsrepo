# Primeros pasos con Docker
> Instalar Portainer (Docker GUI)
> - docker volume create portainer_data
> - docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

-------

| **Comando**                                                                | **Descripcion**                                          | **Nota**                                                                   |
| -------------------------------------------------------------------------- | -------------------------------------------------------- | -------------------------------------------------------------------------- |
| docker ps                                                                  | Listar contenedor                                        |                                                                            |
| docker ps -a                                                               | Ver contenedores activos y no activos                    |                                                                            |
| docker ps -l                                                               | Ver el ultimo contenedor que se creó                     |                                                                            |
| docker stop idContenedor                                                   | Parar contenedor                                         |                                                                            |
| docker start idContenedor                                                  | Iniciar contenedor                                       |                                                                            |
| docker images                                                              | Listar imagenes                                          |                                                                            |
| docker login -u josexs                                                     | Logarse en Dockerhub                                     |                                                                            |
| docker build -t josexs/ubuntu:v1 .                                         | Crear imagen a partir de Dockerfile                      |                                                                            |
| docker push josexs/docker-image-name                                       | Subir imagen a DockerHub                                 | Tienes que estar logado                                                    |
| docker commit -m "Commit Text" -a "JoseXS" container-id josexs/ubuntunode8 | Guardar cambios en nueva imagen                          |                                                                            |
| docker run -it nombreImagen                                                | Ejecutar contenedor (-it te da acceso a shell)           |                                                                            |
| docker rm idContenedor                                                     | Eliminar contenedor                                      | El contenedor tiene que estar parado                                       |
| docker rmi idImagen                                                        | Eliminar imagen                                          |                                                                            |
| docker system prune -a                                                     | Eliminar todo                                            |                                                                            |
| docker-compose up                                                          | Crear contenedor en base a un archivo docker-compose.yml | Tienes que estar en la misma ruta que el archivo. Tambien probar **build** |
| docker-compose down                                                        | Detener contenedores creados con docker compose          |                                                                            |
----------------------------

# Servicios (Swarm)
| **Comando**             | **Descripcion**                              |
| ----------------------- | -------------------------------------------- |
| docker service create   | Crear servicio                               |
| docker service inspect  | Mostrar informacion detallada de un servicio |
| docker service logs     | Obtener el log de los servicios              |
| docker service ls       | Lista de servicios                           |
| docker service ps       | List the tasks of one or more services       |
| docker service rm       | Eliminar servicio                            |
| docker service rollback | Revert changes to a service’s configuration |
| docker service scale    | Scale one or multiple replicated services    |
| docker service update   | Update a service                             |

----

# Docker Compose
| **Comando**                        | **Descripcion**                                   |
| ---------------------------------- | ------------------------------------------------- |
| docker-compose up                  | Crear y levantar contenedor                       |
| docker-compose up --force-recreate | Crear y levantar contenedor borrando cache        |
| docker-compose build --force-rm    | Recrea el contenedor forzando el borrado de cache |

----

# Hacer autoarrancable un contenedor ya creado 
docker update --restart=always idContenedor

# Evitar sudo
Con este comando nos olvidamos de añadir sudo a cada comando de docker. Si no funciona ahi que reiniciar.

`sudo usermod -a -G docker $USER`

--------

[Volver](./README.md)