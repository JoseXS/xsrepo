# Mean Stack en Docker

- Creamos **Dockerfile**
```
FROM node:carbon
WORKDIR /app
COPY package.json ./
RUN npm install --quiet
COPY . .
RUN npm install nodemon -g --quiet
EXPOSE 3000
CMD [ "npm", "start" ]

```
- Creamos **docker-compose.yml**

```
version: "2"
services:
  app:
    build: .
    volumes:
      - ./:/app
    ports:
      - "3000:3000"
    links:
      - mongo

  mongo:
    image: mongo
    ports:
      - "27017:27017"
      
```

## Ejecutar docker-composer.yml
 - docker-compose up


## Borrar cache
- docker-compose up --force-recreate

----

[Volver](./README.md)
