### xshost.ddns.net

| **Nombre**              | **Puerto Interno** | **Puerto Externo** | **Ip Interna** | **Url**                     |
| ----------------------- | ------------------ | ------------------ | -------------- | --------------------------- |
| **ESXI**                | 443                | 443                | 192.168.1.111  | https://xshost.ddns.net     |
| **Portainer**           | 9999               | 1411               | 192.168.1.96   | http://xshost.ddns.net:1411 |
| **Sonar**               | 9000               | 1068               | 192.168.1.96   | http://xshost.ddns.net:1068 |
| **Jenkins**             | 8080               | 1250               | 192.168.1.96   | http://xshost.ddns.net:1417 |
| **Conexion Remota WIN** | 3389               | 1693               | 192.168.1.124  | http://xshost.ddns.net:1417 |

----------------------------

[Volver](./README.md)
