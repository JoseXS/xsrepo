# Comandos principales
| Comando      | Descripcion        |
| ------------ | ------------------ |
| mongo        | conectarse a Mongo |
| use nombreBD | Cambiar de BD      |
| use dbs      | Listar BDs         |

# Seleccionando Base de datos
| Comando                                      | Descripcion                |
| -------------------------------------------- | -------------------------- |
| show collections                             | Listar colecciones         |
| db.pruebas.insert({ name: 'Pepe'});          | Insertar registro          |
| db.pruebas.find()                            | Buscar registro            |
| db.pruebas.update()                          | Actualizar registro        |
| db.pruebas.remove()                          | Borrar registro            |
| db.pruebas.count()                           | Contar registros           |
| db.getCollection('nameCollection≥').find({}) | Listar todos los registros |
| db.dropDatabase()                            | Borrar BD                  |


# Exportar Collection
- mongoexport --db zoo --collection users --out user.json


# Exportar DB
- mongodump -d zoo -o C:/


# Restore DB
- mongorestore -d <database_name> <directory_backup>
	