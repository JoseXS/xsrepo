| Device Name          | Width | Height | DPR | Example User Agent                                                |
| -------------------- | ----- | ------ | --- | ----------------------------------------------------------------- |
| Samsung Galaxy S7    | 360   | 640    | 4   | Mozilla/5.0 (Linux; Android 6.0.1; SM-G935V Build/MMB29M)         |
| Samsung Galaxy S8    | 360   | 740    | 3   | Mozilla/5.0 (Linux; Android 7.0; SM-G950U Build/NRD90M)           |
| Samsung Galaxy S8+   | 412   | 846    | 3.5 | Mozilla/5.0 (Linux; Android 7.0; SM-G955U Build/NRD90M)           |
| iPhone 7             | 375   | 667    | 2   | Mozilla/5.0 (iPhone; CPU iPhone OS 11_1_1 like Mac OS X)          |
| iPhone 7 Plus        | 414   | 736    | 3   | Mozilla/5.0 (iPhone; CPU iPhone OS 11_1_1 like Mac OS X)          |
| iPhone 8             | 375   | 667    | 2   | Mozilla/5.0 (iPhone; CPU iPhone OS 11_1 like Mac OS X)            |
| iPhone 8 Plus        | 414   | 736    | 3   | Mozilla/5.0 (iPhone; CPU iPhone OS 11_1 like Mac OS X)            |
| iPhone X             | 375   | 812    | 3   | Mozilla/5.0 (iPhone; CPU iPhone OS 11_1 like Mac OS X)            |
| iPad Mini 4          | 768   | 1024   | 2   | Mozilla/5.0 (iPad; CPU OS 11_1 like Mac OS X)                     |
| iPad Pro (10.5")     | 834   | 1112   | 2   | Mozilla/5.0 (iPad; CPU OS 11_1 like Mac OS X)                     |
| iPad Pro (12.9")     | 1024  | 1366   | 2   | Mozilla/5.0 (iPad; CPU OS 11_1 like Mac OS X)                     |
| iPhone SE            | 320   | 568    | 2   | Mozilla/5.0 (iPhone; CPU iPhone OS 9_3 like Mac OS X)             |
| Samsung Galaxy Note8 | 412   | 846    | 3.5 | Mozilla/5.0 (Linux; Android 7.1.1; SAMSUNG SM-N950U Build/NMF26X) |

----------------------------

[Volver](./README.md)